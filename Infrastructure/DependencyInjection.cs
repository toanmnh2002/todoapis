﻿using Infrastructure.MapperConfiguration;
using Infrastructure.Repostiories;
using Microsoft.Extensions.DependencyInjection;
using Services.Extensions;
using Services.Interfaces;
using Services.Interfaces.IRepositories;
using System.Text.Json.Serialization;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection RegisterService(this IServiceCollection services)
        {

            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                options.JsonSerializerOptions.WriteIndented = true;
                options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            });


            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITodoRepository, TodoRepository>();
            services.AddScoped<IJwtHelper, JwtHelper>();
            services.AddScoped<IClaimService, ClaimService>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddAutoMapper(typeof(MapperConfigurations).Assembly);

            return services;
        }
    }
}
