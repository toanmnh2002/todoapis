﻿using Services.Models;

namespace Services.Interfaces.IServices
{
    public interface IUserService
    {
        Task<Response<UserModel>> GetUserByIdAsync(int id);
        Task<Response<UserModel>> GetUserByUserNameAsync(string username);
        Task<Response<List<UserModel>>> GetUsersAsync(int pageIndex = 1, int pageSize = 10);
        Task<Response<string>> LoginAsync(LoginModel model);
        Task<Response<UserModel>> RegisterUserAsync(CreateUserModel model);
    }
}