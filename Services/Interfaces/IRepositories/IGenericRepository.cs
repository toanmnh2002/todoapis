﻿using Domain.Enums;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Services.Interfaces.IRepositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task AddEntityAsync(TEntity entity);
        void AddRangeAsync(IEnumerable<TEntity> entities);
        Task<bool> Any(Expression<Func<TEntity, bool>>? expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);
        Task<List<TEntity>> Filter(Expression<Func<TEntity, bool>>? expression = null, Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null, int pageIndex = 1, int pageSize = 10, Expression<Func<TEntity, object>>? sortColumn = null, SortDirection sortDirection = SortDirection.Descending);
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity?> GetByIdAsync(object Id);
        Task<TEntity?> GetEntityByCondition(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
    }
}