﻿using AutoMapper;
using Domain.Enitites;
using Services.Models;

namespace Infrastructure.MapperConfiguration
{
    public class MapperConfigurations : Profile
    {
        public MapperConfigurations()
        {
            CreateMap<CreateTodoRequest, Todo>().ReverseMap();
            CreateMap<UpdateTodoRequest, Todo>().ReverseMap();
            CreateMap<ToDoModel, Todo>().ReverseMap();
            CreateMap<UserModel, User>().ReverseMap();
            CreateMap<CreateUserModel, User>().ReverseMap();
        }
    }
}
