﻿namespace Services.Interfaces
{
    public interface IClaimService
    {
        int? GetCurrentUserId { get; }
        public string? GetCurrentUserUsername { get; }
    }
}
