﻿using AutoMapper;
using Domain.Enitites;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.Interfaces.IServices;
using Services.Models;
using System.Net;

namespace Services.Services
{
    public class TodoService : ITodoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimService _claimService;

        public TodoService(IUnitOfWork unitOfWork, IMapper mapper, IClaimService claimService)
            => (_unitOfWork, _mapper, _claimService) = (unitOfWork, mapper, claimService);

        public async Task<Response<List<Todo>>> GetTodoesAsync(int pageIndex = 1, int pageSize = 10)
        {
            var data = await _unitOfWork.TodoRepository.GetTodoes(pageIndex, pageSize);

            if (data is null || data.Count is 0) return new Response<List<Todo>>(errors: "List is empty", statusCode: 404);

            return new Response<List<Todo>>(result: data);
        }

        public async Task<Response<ToDoModel>> GetTodoeByIdAsync(int id)
        {
            var data = await _unitOfWork.TodoRepository.GetEntityByCondition(x => x.Id == id, x => x.Include(x => x.User));

            if (data is null)
            {
                return new Response<ToDoModel> { Code = HttpStatusCode.NotFound, errors = "Not found todo!" };
            }
            return new Response<ToDoModel> { Code = HttpStatusCode.OK, result = _mapper.Map<ToDoModel>(data) };
        }

        public async Task<Response<Todo>> AddTodoAsync(CreateTodoRequest model)
        {
            var currentUser = _claimService.GetCurrentUserId;
            if (currentUser is null || currentUser is 0)
            {
                return new Response<Todo> { Code = HttpStatusCode.Unauthorized, errors = "Not login yet!" };
            }

            var todo = _mapper.Map<Todo>(model);
            todo.UserId = currentUser.Value;
            await _unitOfWork.TodoRepository.AddEntityAsync(todo);

            var isSucess = await _unitOfWork.SaveChangesAsync();

            if (isSucess > 0)
            {
                return new Response<Todo>
                {
                    Code = HttpStatusCode.OK,
                    result = todo
                };
            }
            return new Response<Todo>
            {
                Code = HttpStatusCode.BadRequest,
                errors = "Add todo fail!"
            };
        }

        public async Task<Response<Todo>> Update(int id, UpdateTodoRequest model)
        {
            var currentUser = _claimService.GetCurrentUserId;
            if (currentUser is null || currentUser is 0)
            {
                return new Response<Todo> { Code = HttpStatusCode.Unauthorized, errors = "Not login yet!" };
            }

            var todo = await _unitOfWork.TodoRepository.GetByIdAsync(id);

            if (todo is null)
            {
                return new Response<Todo>
                {
                    Code = HttpStatusCode.NotFound,
                    errors = "Not found todo!"
                };
            }
            _mapper.Map(model, todo);
            _unitOfWork.TodoRepository.Update(todo);

            var isSuccess = await _unitOfWork.SaveChangesAsync();
            if (isSuccess > 0)
            {
                return new Response<Todo>
                {
                    Code = HttpStatusCode.OK,
                    result = todo,
                };
            }
            return new Response<Todo>
            {
                Code = HttpStatusCode.BadRequest,
                errors = "Update fail!",
            };
        }

        public async Task<Response<Todo>> Delete(int id)
        {
            var currentUser = _claimService.GetCurrentUserId;
            if (currentUser is null || currentUser is 0)
            {
                return new Response<Todo> { Code = HttpStatusCode.Unauthorized, errors = "Not login yet!" };
            }

            var todo = await _unitOfWork.TodoRepository.GetByIdAsync(id);
            if (todo is null)
            {
                return new Response<Todo>
                {
                    Code = HttpStatusCode.NotFound,
                    errors = "Not found!"
                };
            }

            _unitOfWork.TodoRepository.Remove(todo);
            var isSuccess = await _unitOfWork.SaveChangesAsync();
            if (isSuccess > 0)
            {
                return new Response<Todo>
                {
                    Code = HttpStatusCode.OK,
                    result = todo,
                };
            }
            return new Response<Todo>
            {
                Code = HttpStatusCode.BadRequest,
                errors = "Delete fail!",
            };
        }
    }
}
