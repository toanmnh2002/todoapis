﻿using Domain.Enitites;
using Microsoft.Extensions.Configuration;

namespace Services.Interfaces
{
    public interface IJwtHelper
    {
        string GenerateToken(User user, IConfiguration configuration);
        string Hash(string password, string salt);
        string Salt();
        bool Verify(string password, string stringVerify);
    }
}