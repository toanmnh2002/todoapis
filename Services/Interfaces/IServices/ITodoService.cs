﻿using Domain.Enitites;
using Services.Models;

namespace Services.Interfaces.IServices
{
    public interface ITodoService
    {
        Task<Response<Todo>> AddTodoAsync(CreateTodoRequest model);
        Task<Response<Todo>> Delete(int id);
        Task<Response<ToDoModel>> GetTodoeByIdAsync(int id);
        Task<Response<List<Todo>>> GetTodoesAsync(int pageIndex = 1, int pageSize = 10);
        Task<Response<Todo>> Update(int id, UpdateTodoRequest model);
    }
}