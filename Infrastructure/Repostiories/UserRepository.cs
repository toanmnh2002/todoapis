﻿using Domain.Enitites;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces.IRepositories;

namespace Infrastructure.Repostiories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<User>> GetUsers(int pageIndex = 0, int pageSize = 10) => await _dbSet
            .Include(x => x.Todos)
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize)
            .AsNoTracking()
            .ToListAsync();

    }
}
