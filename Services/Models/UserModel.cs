﻿using System.ComponentModel.DataAnnotations;

namespace Services.Models
{
    public class CreateUserModel
    {
        [Required, MinLength(3), RegularExpression(@"^[a-zA-Z]+$"), DataType(DataType.Text)]
        public string Username { get; set; }
        [Required, Length(5, 20), DataType(DataType.Password)]
        public string Password { get; set; }
        [Required, Length(5, 20), Compare("Password", ErrorMessage = "Confirm password must match the password"), DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        [Required, MinLength(3), RegularExpression(@"^[a-zA-Z]+$"), DataType(DataType.Text)]
        public string FullName { get; set; }
        [Required, RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"), Length(0, 100), DataType(DataType.EmailAddress), EmailAddress()]
        public string Email { get; set; }
        [Required, StringLength(10), RegularExpression(@"^(84|0[3|5|7|8|9])+([0-9]{8})$"), DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [MinLength(5), DataType(DataType.Text)]
        public string? Address { get; set; }
    }
    public class UserModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string? Address { get; set; }
    }
}
