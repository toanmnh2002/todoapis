﻿namespace Services.Models
{
    public class AppConfiguration
    {
        public class JwtSection
        {
            public string Key { get; set; }
            public int ExpiresInMinutes { get; set; }
        }

        public class JwtConfiguration
        {
            public JwtSection JwtSection { get; set; }
        }
    }
}
