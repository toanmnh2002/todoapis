﻿using Microsoft.AspNetCore.Http;
using Services.Interfaces;

namespace Services.Extensions
{
    public class ClaimService : IClaimService
    {
        public ClaimService(IHttpContextAccessor httpContextAccessor)
        {
            //var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("UserId");
            var Id = httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(x => x.Value == "UserId")?.Value;
            var username = httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(x => x.Value == "Username")?.Value;
            GetCurrentUserId = string.IsNullOrWhiteSpace(Id) ? 0 : int.Parse(Id);
            GetCurrentUserUsername = string.IsNullOrWhiteSpace(username) ? "Unknown" : username;
        }

        public int? GetCurrentUserId { get; }
        public string? GetCurrentUserUsername { get; }
    }

}
