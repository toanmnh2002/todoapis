﻿using Services.Interfaces.IRepositories;

namespace Infrastructure
{
    public interface IUnitOfWork
    {
        public ITodoRepository TodoRepository { get; }
        public IUserRepository UserRepository { get; }
        Task<int> SaveChangesAsync();
    }
}
