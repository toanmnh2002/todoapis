﻿using Domain.Enitites;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces.IRepositories;

namespace Infrastructure.Repostiories
{
    public class TodoRepository : GenericRepository<Todo>, ITodoRepository
    {
        public TodoRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Todo>> GetTodoes(int pageIndex = 1, int pageSize = 10) => await _dbSet
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize).AsNoTracking()
            .ToListAsync();
    }
}
