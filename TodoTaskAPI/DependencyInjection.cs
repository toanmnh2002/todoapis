﻿using Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Services.Extensions;
using Services.Interfaces;
using Services.Interfaces.IServices;
using Services.Services;
using System.Text;
using static Services.Models.AppConfiguration;

namespace TodoTaskAPI
{
    public static class DepenedencyInjection
    {
        public static IServiceCollection RegisterCoreServices(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddEndpointsApiExplorer();
            services.AddHttpContextAccessor();
            services.AddSwaggerGen();
            services.AddAuthorization();

            //services.AddSingleton(configuration.GetSection("JwtSection").Get<JwtSection>());

            //services.AddSingleton(configuration.Get<AppConfiguration>());

            services.Configure<JwtSection>(configuration.GetSection(nameof(JwtSection)));

            services.AddDbContext<AppDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("db")));

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITodoService, TodoService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IJwtHelper, JwtHelper>();
            services.AddScoped<IClaimService, ClaimService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                  {
                      options.TokenValidationParameters = new TokenValidationParameters
                      {
                          ValidateIssuer = false,
                          ValidateAudience = false,
                          ValidateLifetime = false,
                          ValidateIssuerSigningKey = false,
                          IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSection:Key"]!)),
                      };
                  });

            return services;
        }
    }
}
/*
 var jwtSection = builder.Configuration.GetSection("JWTSection").Get<JWTSection>();
builder.Services.AddSingleton(jwtSection);

public class JWTSection
{
public string SecretKey { get; set; }
public int ExpiresInDays { get; set; }
}

var jwtSection=builder.Configuration.Get<AppConfig>();
builder.Services.AddSingleton(jwtSection);
public class AppConfig
{
public JWTSection JWTSection {get;set;}
}

var jwtSection=builder.Configuration.GetSection("JWTSection");
builder.Services.
Configure<JWTSection>(
builder.Configuration.GetSection("JWTSection")
);

private readonly JwtSection _jwtSection;
public Service(IOptions<JWTSection> jwtSection)=>(_jwtSection)=(jwtSection.Value);

configuration["Section:key"]
 */