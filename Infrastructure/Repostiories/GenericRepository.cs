﻿using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Services.Interfaces.IRepositories;
using System.Linq.Expressions;

namespace Infrastructure.Repostiories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected DbSet<TEntity> _dbSet;

        public GenericRepository(AppDbContext context) => _dbSet = context.Set<TEntity>();

        public async Task<List<TEntity>> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();

        public async Task<TEntity?> GetByIdAsync(object Id) => await _dbSet.FindAsync(Id);

        public async Task AddEntityAsync(TEntity entity) => await _dbSet.AddAsync(entity);

        public async void AddRangeAsync(IEnumerable<TEntity> entities) => await _dbSet.AddRangeAsync(entities);

        public void Update(TEntity entity) => _dbSet.Update(entity);

        public void UpdateRange(IEnumerable<TEntity> entities) => _dbSet.UpdateRange(entities);

        public void Remove(TEntity entity) => _dbSet.Remove(entity);

        public void RemoveRange(IEnumerable<TEntity> entities) => _dbSet.RemoveRange(entities);

        public async Task<TEntity?> GetEntityByCondition(Expression<Func<TEntity, bool>> expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null)
        {
            var query = _dbSet.AsQueryable();

            if (include is not null)
            {
                query = include(query);
            }
            return await query.FirstOrDefaultAsync(expression);
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>>? expression, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null)
        {
            var query = _dbSet.AsQueryable();

            if (include is not null)
            {
                query = include(query);
            }
            return await query.AnyAsync(expression);
        }

        public async Task<List<TEntity>> Filter(Expression<Func<TEntity, bool>>? expression = null, Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null, int pageIndex = 1, int pageSize = 10, Expression<Func<TEntity, object>>? sortColumn = null, SortDirection sortDirection = SortDirection.Descending)
        {
            var query = _dbSet.AsQueryable();

            if (include is not null)
            {
                query = include(query);
            }

            if (expression is not null)
            {
                query = query.Where(expression);
            }

            if (sortColumn is not null)
            {
                query = sortDirection == SortDirection.Ascending
                    ? query.OrderBy(sortColumn)
                    : query.OrderByDescending(sortColumn);
            }

            var result = await query
                              .Skip((pageIndex - 1) * pageSize)
                              .Take(pageSize)
                              .ToListAsync();

            return result;
        }

    }
}
