﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces.IServices;
using Services.Models;
using System.Net;

namespace TodoTaskAPI.Controllers
{
    [Route("/todoTasks")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly ITodoService _todoService;

        public TodoController(ITodoService todoService) => _todoService = todoService;

        [HttpGet]
        public async Task<ActionResult> GetTodos(int pageIndex = 1, int pageSize = 10)
        {
            var result = await _todoService.GetTodoesAsync(pageIndex, pageSize);
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> AddTodo(CreateTodoRequest model)
        {

            var result = await _todoService.AddTodoAsync(model);
            if (result.Code is HttpStatusCode.Unauthorized) return Unauthorized();
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            return Ok(result);
        }

        [HttpPut]
        public async Task<ActionResult> UpdateTodo(int id, UpdateTodoRequest model)
        {

            var result = await _todoService.Update(id, model);
            if (result.Code is HttpStatusCode.Unauthorized) return Unauthorized();
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            if (result.Code is HttpStatusCode.BadRequest) return BadRequest();
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTodo(int id)
        {

            var result = await _todoService.Delete(id);
            if (result.Code is HttpStatusCode.Unauthorized) return Unauthorized();
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            if (result.Code is HttpStatusCode.BadRequest) return BadRequest();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetTodo(int id)
        {
            var result = await _todoService.GetTodoeByIdAsync(id);
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            return Ok(result);
        }
    }
}
