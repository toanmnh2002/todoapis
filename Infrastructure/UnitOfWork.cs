﻿using Services.Interfaces.IRepositories;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IUserRepository _userRepository;
        private readonly ITodoRepository _todoRepository;

        public UnitOfWork(AppDbContext context, IUserRepository userRepository, ITodoRepository todoRepository)
        {
            _context = context;
            _userRepository = userRepository;
            _todoRepository = todoRepository;
        }

        public ITodoRepository TodoRepository => _todoRepository;

        public IUserRepository UserRepository => _userRepository;

        public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();
    }
}
