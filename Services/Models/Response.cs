﻿using System.Net;

namespace Services.Models
{
    public class Response<T>
    {
        public HttpStatusCode Code { get; set; }
        public string? errors { get; set; }
        public T? result { get; set; }

        public Response() { }
        public Response(T? result)
        {
            Code = HttpStatusCode.NotFound;
            this.result = result;
        }

        public Response(string? errors, int statusCode)
        {
            Code = (HttpStatusCode)statusCode;
            this.errors = errors;
        }

    }
}
