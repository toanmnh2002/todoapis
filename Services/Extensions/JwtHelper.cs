﻿using Domain.Enitites;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Services.Interfaces;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services.Extensions
{
    public class JwtHelper : IJwtHelper
    {
        public string Hash(string password, string salt) => BCrypt.Net.BCrypt.HashPassword(password, salt);
        public bool Verify(string password, string stringVerify) => BCrypt.Net.BCrypt.Verify(password, stringVerify);
        public string Salt() => BCrypt.Net.BCrypt.GenerateSalt();
        public string GenerateToken(User user, IConfiguration configuration)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSection:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var expiredToken = DateTime.UtcNow.AddDays(1);
            var claims = new[]
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim("Email", user.Email),
                new Claim("Username", user.Username),
                new Claim("Phone", user.Phone),
                new Claim(JwtRegisteredClaimNames.Exp,expiredToken.ToString("yyyy/MM/dd hh:mm:ss"),ClaimValueTypes.String),
            };
            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: expiredToken,
                    signingCredentials: credentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
