﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces.IServices;
using Services.Models;
using System.Net;

namespace TodoTaskAPI.Controllers
{
    [Route("users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login(LoginModel loginModel)
        {
            var result = await _userService.LoginAsync(loginModel);
            if (result.Code is HttpStatusCode.NotFound) return NotFound();

            return Ok(result);
        }

        [HttpPost]
        [Route("register")]
        public async Task<ActionResult> Register(CreateUserModel model)
        {
            var result = await _userService.RegisterUserAsync(model);
            if (result.Code is HttpStatusCode.BadRequest) return BadRequest();
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult> GetUsers(int pageIndex = 1, int pageSize = 10)
        {
            var result = await _userService.GetUsersAsync(pageIndex, pageSize);
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            return Ok(result);
        }

        [HttpGet(":{id}")]
        public async Task<ActionResult> GetUserById(int id)
        {
            var result = await _userService.GetUserByIdAsync(id);
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            return Ok(result);
        }

        [HttpGet("username/{username}")]
        public async Task<ActionResult> GetUserByUsername(string username)
        {
            var result = await _userService.GetUserByUserNameAsync(username);
            if (result.Code is HttpStatusCode.NotFound) return NotFound();
            return Ok(result);
        }

    }
}
