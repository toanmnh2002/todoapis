﻿using Domain.Enitites;

namespace Services.Interfaces.IRepositories
{
    public interface ITodoRepository : IGenericRepository<Todo>
    {
        Task<List<Todo>> GetTodoes(int pageIndex = 1, int pageSize = 10);
    }
}
