﻿using Domain.Enitites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.FluentAPIs
{
    public class Configurations
    {
        public class UserConfiguration : IEntityTypeConfiguration<User>
        {
            public void Configure(EntityTypeBuilder<User> builder)
            {
                builder.HasKey(x => x.Id);
                builder.HasIndex(x => x.Email).IsUnique();
                builder.HasIndex(x => x.Phone).IsUnique();

                builder.HasMany(x => x.Todos)
                    .WithOne(x => x.User)
                    .HasForeignKey(x => x.UserId);

                builder.HasData(new User
                {
                    Id = 1,
                    Email = "Toanmnh2002@gmail.com",
                    Password = "$2a$11$UjgjAaLNMMRJ92D91fEyR.dOjn43uIBZppj.RTvSBiLW0deACJai6",
                    ConfirmPassword = "123456",
                    Address = "Australia",
                    Username = "ToanNM",
                    Phone = "08692743xx",
                    FullName = "ToanNguyen"
                }, new User
                {
                    Id = 2,
                    Email = "Toanmanh2002@gmail.com",
                    Password = "$2a$11$UjgjAaLNMMRJ92D91fEyR.dOjn43uIBZppj.RTvSBiLW0deACJai6",
                    ConfirmPassword = "123456",
                    Address = "Australia",
                    Username = "ToanNM32",
                    Phone = "0869274xxx",
                    FullName = "ToanNguyenManh"
                });

            }
        }

        public class TodoConfiguration : IEntityTypeConfiguration<Todo>
        {
            public void Configure(EntityTypeBuilder<Todo> builder)
            {
                builder.HasKey(x => x.Id);

                builder.HasData(new Todo
                {
                    Id = 1,
                    Description = "helo",
                    Title = "hola",
                    UserId = 1,
                }, new Todo
                {
                    Id = 2,
                    Description = "merci",
                    Title = "konichiwa",
                    UserId = 2,
                });

            }
        }
    }
}
