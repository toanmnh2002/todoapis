﻿using System.ComponentModel.DataAnnotations;

namespace Services.Models
{
    public class CreateTodoRequest
    {
        [Required, MinLength(3)]
        public string? Description { get; set; }
        [Required, MinLength(3)]
        public string? Title { get; set; }
    }
    public class UpdateTodoRequest
    {
        [MinLength(3)]
        public string? Description { get; set; }
        [MinLength(3)]
        public string? Title { get; set; }
    }
    public class ToDoModel
    {
        public int Id { get; set; }
        public string? Description { get; set; }
        public string Title { get; set; }
        public UserModel User { get; set; }
        public int UserId { get; set; }
    }

}
