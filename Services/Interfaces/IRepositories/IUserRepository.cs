﻿using Domain.Enitites;

namespace Services.Interfaces.IRepositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<List<User>> GetUsers(int pageIndex = 0, int pageSize = 10);
    }
}
