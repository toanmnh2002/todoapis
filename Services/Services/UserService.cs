﻿using AutoMapper;
using Domain.Enitites;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Services.Interfaces;
using Services.Interfaces.IServices;
using Services.Models;
using System.Net;

namespace Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IJwtHelper _jwtHelper;
        private readonly IConfiguration _configuration;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, IJwtHelper jwtHelper, IConfiguration configuration)
        => (_unitOfWork, _mapper, _jwtHelper, _configuration) = (unitOfWork, mapper, jwtHelper, configuration);


        public async Task<Response<List<UserModel>>> GetUsersAsync(int pageIndex = 1, int pageSize = 10)
        {
            var data = await _unitOfWork.UserRepository.GetUsers(pageIndex, pageSize);

            if (data is null || data.Count is 0)
            {
                return new Response<List<UserModel>> { Code = HttpStatusCode.NotFound, errors = "List is empty!" };
            }
            return new Response<List<UserModel>> { Code = HttpStatusCode.OK, result = _mapper.Map<List<UserModel>>(data) };
        }

        public async Task<Response<UserModel>> GetUserByIdAsync(int id)
        {

            var data = await _unitOfWork.UserRepository.GetByIdAsync(id);

            if (data is null)
            {
                return new Response<UserModel> { Code = HttpStatusCode.NotFound, errors = "Not found user!" };
            }
            return new Response<UserModel> { Code = HttpStatusCode.OK, result = _mapper.Map<UserModel>(data) };
        }

        public async Task<Response<string>> LoginAsync(LoginModel model)
        {
            var userLogin = await _unitOfWork.UserRepository.GetEntityByCondition(x => x.Email == model.Email);

            if (userLogin is null || !_jwtHelper.Verify(model.Password, userLogin.Password))
            {
                return new Response<string> { Code = HttpStatusCode.NotFound, errors = "Email and password is incorrect!" };
            }
            return new Response<string> { Code = HttpStatusCode.OK, result = _jwtHelper.GenerateToken(userLogin, _configuration) };
        }

        public async Task<Response<UserModel>> GetUserByUserNameAsync(string username)
        {
            var user = await _unitOfWork.UserRepository.GetEntityByCondition(x => x.Username == username, x => x.Include(x => x.Todos));

            if (user is null)
            {
                return new Response<UserModel> { Code = HttpStatusCode.NotFound, errors = "Not found user!" };
            }
            return new Response<UserModel> { Code = HttpStatusCode.OK, result = _mapper.Map<UserModel>(user) };

        }

        public async Task<Response<UserModel>> RegisterUserAsync(CreateUserModel model)
        {

            var user = _mapper.Map<User>(model);
            user.Password = _jwtHelper.Hash(user.Password, _jwtHelper.Salt());
            user.ConfirmPassword = _jwtHelper.Hash(user.ConfirmPassword, _jwtHelper.Salt());
            await _unitOfWork.UserRepository.AddEntityAsync(user);

            var isSucess = await _unitOfWork.SaveChangesAsync();

            if (isSucess > 0)
            {
                return new Response<UserModel>
                {
                    Code = HttpStatusCode.OK,
                    result = _mapper.Map<UserModel>(user)
                };
            }
            return new Response<UserModel>
            {
                Code = HttpStatusCode.BadRequest,
                errors = "Register user fail!"
            };

        }

    }

}
